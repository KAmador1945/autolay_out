//
//  SwipeModel.swift
//  AutoLayout
//
//  Created by Kevin Amador Rios on 6/21/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import Foundation

struct SwipeModel {
    
    let imageName: String
    let title: String
    let bodyText: String
}
