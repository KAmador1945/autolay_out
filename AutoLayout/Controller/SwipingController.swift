//
//  SwipingController.swift
//  AutoLayout
//
//  Created by Kevin Amador Rios on 6/21/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

class SwipingController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    //Access to model
    let swipeModel = [
        SwipeModel(imageName: "bear_first", title: "Welcome to E-Pics!", bodyText: "The must powerful social network"),
        SwipeModel(imageName: "heart_second", title: "Privacy", bodyText: "We respect and protect your privacy"),
        SwipeModel(imageName: "leaf_third", title: "Account", bodyText: "Join us and create your account")
    ]
    
    
    let previusButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Previus", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 18)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.tintColor = UIColor.gray
        btn.addTarget(self, action: #selector(HandPrev), for: .touchUpInside)
        return btn
    }()
    
    let nextButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Next", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 18)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.tintColor = UIColor.purple
        btn.addTarget(self, action: #selector(HandNext), for: .touchUpInside)
        return btn
    }()
    
    //Control page
    private lazy var pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = swipeModel.count
        pc.currentPageIndicatorTintColor = .purple
        pc.pageIndicatorTintColor = .gray
        return pc
    }()

    //Set id
    let cell_id = "cell_id"
    
    //First function on load
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView.backgroundColor = .white
        
        //Register cell
        collectionView.register(SwipingCell.self, forCellWithReuseIdentifier: cell_id)
        
        //enable swiping
        collectionView.isPagingEnabled = true
        
        //Call function
        SetBtn()
    }
    
    //Return number of cell
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return swipeModel.count
    }
    
    //Access to cell controller
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell_id", for: indexPath) as! SwipingCell
        
        //Access to array model
        let swipes = swipeModel[indexPath.item]
        cell.swipesModel = swipes
    
        return cell
    }
    
    //Define size of cell
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    //Define the spacing
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let x = targetContentOffset.pointee.x
        pageControl.currentPage = Int(x / view.frame.width)
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        
        coordinator.animate(alongsideTransition: { (_) in
            self.collectionViewLayout.invalidateLayout()
            let indexPath = IndexPath(item: self.pageControl.currentPage, section: 0)
            self.collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        }) { (_) in
            
        }
        
        collectionViewLayout.invalidateLayout()
    }
    
    //Set btn objects on the main view
    fileprivate func SetBtn() {
        
        //Stackview
        let stackView = UIStackView(arrangedSubviews: [previusButton,pageControl,nextButton])
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.distribution = .fillEqually
        
        //Add to main view stacview
        view.addSubview(stackView)
        
        //Constraint
        NSLayoutConstraint.activate([
            stackView.bottomAnchor.constraint(equalTo: view.safeAreaLayoutGuide.bottomAnchor, constant: 0),
            stackView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor, constant: 0),
            stackView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor, constant: 0),
            stackView.heightAnchor.constraint(equalToConstant: 50)
        ])
    }
    
    //Load another view
    @objc private func HandNext() {
        //Access to item of collectionview
        let nextIndex = min(pageControl.currentPage + 1, swipeModel.count - 1)
        let indexPath = IndexPath(item: nextIndex, section: 0)
        pageControl.currentPage = nextIndex
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
    
    @objc private func HandPrev() {
        //Access to item of collectionview
        let prevIndex = max(pageControl.currentPage - 1, 0)
        let indexPath = IndexPath(item: prevIndex, section: 0)
        pageControl.currentPage = prevIndex
        collectionView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
    }
}
