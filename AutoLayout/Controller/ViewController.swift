//
//  ViewController.swift
//  AutoLayout
//
//  Created by Kevin Amador Rios on 6/21/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    //Images bear
    let ImageviewBear: UIImageView = {
        let img = UIImage(named: "bear_first")
        let imageView = UIImageView(image: img)
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    let previusButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Previus", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 18)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.tintColor = UIColor.gray
        return btn
    }()
    
    let nextButton: UIButton = {
        let btn = UIButton(type: .system)
        btn.setTitle("Next", for: .normal)
        btn.titleLabel?.font = UIFont(name: "Avenir Next", size: 18)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.tintColor = UIColor.purple
        return btn
    }()

    //Control page
    private let pageControl: UIPageControl = {
        let pc = UIPageControl()
        pc.currentPage = 0
        pc.numberOfPages = 4
        pc.currentPageIndicatorTintColor = .purple
        pc.pageIndicatorTintColor = .gray
        return pc
    }()

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        SetObjects() 
    }
    
    //Setting objects on the main screen
    fileprivate func SetObjects() {
        
        //add object to view
        
        //Container view
        let containerView = UIView()
        view.addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        
        //Constraint view
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: view.topAnchor),
            containerView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            containerView.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 0.5)
        ])
        
        //add object to second view
        containerView.addSubview(ImageviewBear)
        
        //Constraint
        NSLayoutConstraint.activate([
            ImageviewBear.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            ImageviewBear.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            ImageviewBear.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.5)
        ])
    }
}

