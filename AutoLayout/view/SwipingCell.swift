//
//  SwipingCell.swift
//  AutoLayout
//
//  Created by Kevin Amador Rios on 6/21/19.
//  Copyright © 2019 Kevin Amador Rios. All rights reserved.
//

import UIKit

class SwipingCell: UICollectionViewCell {
    
    //Access to model
    var swipesModel:SwipeModel? {
        
        didSet {
            
            //Wrap data
            guard let imageSwipes = swipesModel?.imageName else { return }
            guard let textSwipes = swipesModel?.title else { return }
            guard let textBodys = swipesModel?.bodyText else { return }
            
            //Add data to view
            self.Imageview.image = UIImage(named: imageSwipes)
            
            //Set attributes text
            let attributeText = NSMutableAttributedString(string: textSwipes, attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 25)!, NSAttributedString.Key.foregroundColor: UIColor.purple])
            attributeText.append(NSAttributedString(string: "\n\n", attributes: [:]))
            attributeText.append(NSAttributedString(string: textBodys, attributes: [NSAttributedString.Key.font: UIFont(name: "Avenir Next", size: 18)!, NSAttributedString.Key.foregroundColor: UIColor.lightGray]))
            textViews.attributedText = attributeText
            textViews.textAlignment = .center
        }
    }
    
    //Images bear
    private let Imageview: UIImageView = {
        let img = UIImage(named: "bear_first")
        let imageView = UIImageView(image: img)
        imageView.contentMode = .scaleAspectFill
        imageView.translatesAutoresizingMaskIntoConstraints = false
        return imageView
    }()
    
    //Textview
    private let textViews: UITextView = {
        let tv = UITextView()
        tv.isEditable = false
        tv.isScrollEnabled = false
        tv.translatesAutoresizingMaskIntoConstraints = false
        return tv
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        SetObjects()
    }
    
    //Setting objects on the main screen
    fileprivate func SetObjects() {
        
        //Container view
        let containerView = UIView()
        addSubview(containerView)
        containerView.translatesAutoresizingMaskIntoConstraints = false
        
        //Constraint view
        NSLayoutConstraint.activate([
            containerView.topAnchor.constraint(equalTo: topAnchor),
            containerView.leadingAnchor.constraint(equalTo: leadingAnchor),
            containerView.trailingAnchor.constraint(equalTo: trailingAnchor),
            containerView.heightAnchor.constraint(equalTo: heightAnchor, multiplier: 0.5)
        ])
        
        //add object to second view
        containerView.addSubview(Imageview)
        
        //Constraint
        NSLayoutConstraint.activate([
            Imageview.centerXAnchor.constraint(equalTo: containerView.centerXAnchor),
            Imageview.centerYAnchor.constraint(equalTo: containerView.centerYAnchor),
            Imageview.heightAnchor.constraint(equalTo: containerView.heightAnchor, multiplier: 0.5)
        ])
        
        
        //add object to view
        addSubview(textViews)
        
        //Constraint
        NSLayoutConstraint.activate([
            textViews.topAnchor.constraint(equalTo: containerView.bottomAnchor),
            textViews.leftAnchor.constraint(equalTo: leftAnchor),
            textViews.rightAnchor.constraint(equalTo: rightAnchor),
            textViews.bottomAnchor.constraint(equalTo: bottomAnchor, constant: 0),
        ])
    }

    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
